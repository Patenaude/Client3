﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Classes
{
	[Serializable]
    class Message
    {
		public string Auteur { get; private set; }
		public string Contenu { get; private set; }
		public string Temps { get; private set; }

		public Message(string auteur, string contenu, string temps = null)
		{
			Auteur = auteur;
			Contenu = contenu;
			Temps = (temps == null) ? DateTime.Now.ToLongTimeString() : temps;
		}
	}
}
