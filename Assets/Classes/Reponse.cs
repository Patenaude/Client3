﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Classes
{
    class Reponse
    {
		public int ID { get; set; }
		public int IDJoueur { get; private set; }
        public int[] IDCartesBlanches { get; private set; }
        public int IDCarteNoire { get; private set; }

        public Reponse(int joueur, int[] cb, int cn)
        {
            IDJoueur = joueur;
			IDCartesBlanches = cb;
			IDCarteNoire = cn;
        }
    }
}
