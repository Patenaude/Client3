﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;

namespace Assets.Classes
{
	public class NetworkClient : MonoBehaviour
	{
		int serverPort = 11111;
		public int port = 11110;
		public string serverAddress = "127.0.0.1";

		public Button BouttonChat;
		public Text Chat;
		public InputField InputChat;

		int clientSocket = -1;
		int clientConnection = -1;
		byte channelID;
		bool initialized = false;
		bool connected = false;
		bool connecting = false;

		public string nom = "XYZ";
		CarteNoire carteNoire;
		JoueurClient joueur;
		List<JoueurClient> joueurs;
		Reponse[] reponses;

		string separateur = "/~/";

		void Start()
		{
			GlobalConfig gc = new GlobalConfig();
			gc.ReactorModel = ReactorModel.FixRateReactor;
			gc.ThreadAwakeTimeout = 10;

			ConnectionConfig cc = new ConnectionConfig();
			channelID = cc.AddChannel(QosType.ReliableSequenced);

			HostTopology ht = new HostTopology(cc, 1);

			NetworkTransport.Init(gc);

			clientSocket = NetworkTransport.AddHost(ht, port);

			if (clientSocket < 0) { Chat.text += "Client socket creation failed!\n"; }
			else { Chat.text += "Client socket creation successful!\n"; }

			initialized = true;
			joueur = new JoueurClient(nom);

			Connect();
		}

		void Update()
		{
			if (!initialized)
			{
				return;
			}

			int recHostId;
			int connectionId;
			int channelId;
			int dataSize;
			byte[] buffer = new byte[1024];
			byte error;

			NetworkEventType networkEvent = NetworkEventType.DataEvent;

			do
			{
				networkEvent = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, buffer, 1024, out dataSize, out error);

				if(networkEvent == NetworkEventType.Nothing) { }
				else if(networkEvent == NetworkEventType.ConnectEvent)
				{
					Chat.text += "Client: Client connected with id " + connectionId.ToString() + "!\n";
					connected = true;
				}
				else if (networkEvent == NetworkEventType.DataEvent)
				{
					Chat.text += "Client: Received Data from " + connectionId.ToString() + "!\n";
					Stream stream = new MemoryStream(buffer);
					BinaryFormatter f = new BinaryFormatter();
					string msg = f.Deserialize(stream).ToString();
					TraiterMessage(msg);
				}
				else if (networkEvent == NetworkEventType.DisconnectEvent)
				{
					Chat.text += "Client: Disconnected from server!\n";
					connected = false;
				}
			} while (networkEvent != NetworkEventType.Nothing);
		}

		private void TraiterMessage(string message)
		{
			string[] parametres = message.Split(new string[] { separateur }, StringSplitOptions.RemoveEmptyEntries);

			if (parametres[0] == "PartieStarted")
			{
				StartGame();
			}
			else if (parametres[0] == "CartesBlanches")
			{
				int nbr = Int16.Parse(parametres[1]);
				int[] idCartes = new int[nbr];
				for (int x = 0; x < nbr; x++)
				{
					idCartes[x] = Int16.Parse(parametres[x + 2]);
				}
				joueur.AjouterCartes(idCartes);
				//TODO afficher/ajouter/modifier cartes
			}
			else if (parametres[0] == "CarteNoire")
			{ 
				carteNoire = CarteNoire.CartesNoires[Int16.Parse(parametres[1])];
				//TODO afficher nouvelle carte noire
			}
			else if (parametres[0] == "Czar")
			{
				int idCzar = Int16.Parse(parametres[1]);
				if (idCzar == joueur.ID)
				{
					joueur.Czar = true;
				}
				else
				{
					foreach (JoueurClient j in joueurs)
					{
						if (j.ID == idCzar)
						{
							j.Czar = true;
							break;
						}
					}
				}
				//TODO afficher czar
			}
			else if (parametres[0] == "Reponses")
			{
				int nbr = Int16.Parse(parametres[1]);
				reponses = new Reponse[nbr];
				for(int x = 0; x < nbr; x++)
				{
					reponses[x] = (Reponse)DeserializeBinary(parametres[x + 2]);
				}
				//TODO afficher reponses
			}
			else if (parametres[0] == "InfoJoueurs")
			{
				int nbr = Int16.Parse(parametres[1]);
				joueurs = new List<JoueurClient>();
				for (int x = 0; x < nbr; x++)
				{
					joueurs.Add((JoueurClient)DeserializeBinary(parametres[x+2]));
				}
				//TODO mettre a jour affichage
			}
			else if (parametres[0] == "NewJoueur")
			{
				if (joueurs == null)
					joueurs = new List<JoueurClient>();
				joueurs.Add((JoueurClient)DeserializeBinary(parametres[1]));
				//TODO mettre a jour affichage
			}
			else if (parametres[0] == "Message")
			{
				Chat.text += parametres[1] + "\n";
			}
			else if (parametres[0] == "Pret")
			{
				int id = Int16.Parse(parametres[1]);
				foreach(JoueurClient j in joueurs)
				{
					if(j.ID == id)
					{
						j.Pret = (parametres[2] == "true") ? true : false;
						break;
						//TODO modifier affichage
					}
				}
			}
			else if (parametres[0] == "Erreur")
			{
				//TODO afficher erreur dans chat
			}
			else if (parametres[0] == "ID")
			{
				joueur.ID = Int16.Parse(parametres[1]);
				SendInfoJoueur();
			}
		}

		void SendInfoJoueur()
		{
			Chat.text += "envoie InfoJoueur";
			SendData("InfoJoueur" + separateur + SerializeBinary(new Joueur(joueur)));
		}
		void SendPret()
		{
			SendData("Pret" + separateur + joueur.Pret.ToString());
		}
		void SendReponse() { }
		void SendChoix() { }
		void GetInfoJoueurs() { }
		public void SendMessage()
		{
			SendData("Message" + separateur + "[" + joueur.Nom + "]:" + InputChat.text);
			InputChat.text = "";
		}

		void StartGame()
		{
			//TODO implementer
			throw new NotImplementedException();
		}

		void SendData(string message)
		{
			Debug.Log("send data");
			byte error;
			byte[] buffer = new byte[1024];
			Stream stream = new MemoryStream(buffer);
			BinaryFormatter f = new BinaryFormatter();
			f.Serialize(stream, message);

			NetworkTransport.Send(clientSocket, clientConnection, channelID, buffer, (int)stream.Position, out error);

			Chat.text += "Message sent\n";
			LogNetworkError(error);
		}

		void Connect()
		{
			byte error;
			clientConnection = NetworkTransport.Connect(clientSocket, serverAddress, serverPort, 0, out error);

			LogNetworkError(error);
		}

		void Disconnect()
		{
			byte error;
			NetworkTransport.Disconnect(clientSocket, clientConnection, out error);

			LogNetworkError(error);
		}

		public string SerializeBinary(object o)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				new BinaryFormatter().Serialize(ms, o);
				return Convert.ToBase64String(ms.ToArray());
			}
		}

		public object DeserializeBinary(string s)
		{
			byte[] bytes = Convert.FromBase64String(s);
			using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
			{
				ms.Write(bytes, 0, bytes.Length);
				ms.Position = 0;
				return new BinaryFormatter().Deserialize(ms);
			}
		}

		void LogNetworkError(byte error)
		{
			if (error != (byte)NetworkError.Ok)
			{
				NetworkError nerror = (NetworkError)error;
				Chat.text += "Error: " + nerror.ToString() + "\n";
			}
		}
	}
}