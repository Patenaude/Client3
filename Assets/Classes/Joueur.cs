﻿using System;

namespace Assets.Classes
{
	[Serializable]
	class Joueur
	{
		public int ID { get; set; }
		public string Nom { get; private set; }
		public int Score { get; private set; }
		public bool Pret { get; set; }
		public int NbrCartes { get; set; }

		public Joueur(string nom, int id = -1)
		{
			ID = id;
			Nom = nom;
			Pret = false;
			Score = 0;
			NbrCartes = 0;
		}
		public Joueur(JoueurClient j)
		{
			ID = j.ID;
			Nom = j.Nom;
			Score = 0;
			Pret = j.Pret;
			NbrCartes = j.GetNbrCartes();
		}
	}
}
