﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Classes
{
	[Serializable]
	class JoueurClient : Joueur
	{
		public List<CarteBlanche> Cartes { get; set; }
		public bool Czar { get; set; }

		public JoueurClient (string nom, int id = -1) : base(nom, id)
		{
			Cartes = new List<CarteBlanche>(10);
			Czar = false;
		}

		public void AjouterCartes(int[] idCartes)
		{
			foreach(int id in idCartes)
			{
				Cartes.Add(CarteBlanche.CartesBlanches[id]);
			}
		}

		public int GetNbrCartes()
		{
			int x = 0;
			foreach(CarteBlanche cb in Cartes)
			{
				if (cb.ID != -1)
					x++;
			}
			return x;
		}
	}
}
